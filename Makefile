SHELL := /bin/bash

.DEFAULT: freshInstall
.PHONY: freshInstall
freshInstall:
	make remove
	make prepare
	make run
	./utility.sh wait_until_url_available http://localhost:8088
	sleep 1
	make startBrowsers

.PHONY: prepare
prepare:
	curl --silent https://raw.githubusercontent.com/jessfraz/dotfiles/master/etc/docker/seccomp/chrome.json -o ./chromium/chromium.json

.PHONY: run
run:
	docker compose up -d

.PHONY: remove
remove:
	-docker compose down --rmi local

.PHONY: startBrowsers
startBrowsers:
	./utility.sh open_url_when_available http://localhost:8088/vnc.html?autoconnect=true&resize=scale &
	docker exec -itd docker-browsers-novnc-firefox-1 bash -c "/usr/bin/firefox-esr about:blank"
	docker exec -itd docker-browsers-novnc-chromium-1 bash -c "/usr/bin/chromium --start-maximized --no-default-browser-check --no-first-run about:blank"